#!/bin/bash

PROJECT_ID="boa-vista-293222"
STORAGE_CLASS="Standard"
BUCKET_LOCATION="us-central1"
BUCKET_NAME="gs://boavista_storage"

gsutil mb -p $PROJECT_ID \
        -c $STORAGE_CLASS \
        -l $BUCKET_LOCATION \
        -b off $BUCKET_NAME