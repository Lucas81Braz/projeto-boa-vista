# Para enviar os arquivos para o cloud storage iremos instalar o console google-cloud-sdk pois feito uma vez não precisaremos mais realizar conexões para envio dos arquivos para o cloud storage, realizei a instalação para não gerar custos utilizando API ou até mesmo um cluster para ingestão via instancias de vm.

#preparação de ambiente ompremisse (linux).

#comando para instalação do python para utilizar o google-cloud-sdk:
	
	apt-get install python3
	apt-get install python

# Criação dos diretorios para o projeto:
	
	mkdir gcp/
	mkdir gcp/scripts/
	mkdir gcp/logs/

#download do google-cloud-sdk para conexão:
	
	curl -O https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-sdk-313.0.1-linux-x86_64.tar.gz

#descompacte o arquivo no diretorio do projeto:
	
	tar -xzvf google-cloud-sdk-313.0.1-linux-x86_64.tar.gz

#executa a instalação:
	
	./google-cloud-sdk/install.sh

# finalizado a preparação do ambiente ompremisse (linux) para utilizarmos para envio dos arquivos para o cloud storage.

#preparação de ambiente GCP.

#Execute no cloud shell o script de criação do bucket no cloud storage, necessario para armazemanamento dos arquivos.

	./create_bucket.sh

#Utilizei o bucket regional localizado em Iowa(us-central1) e o controle de acesso detalhado pois os custos são menores e escolhi a classe de armazemanamento Standard pois devido ao consumo diario de dados para visualização requerer acessos com frequencias.

#Finalizado a preparação de ambiente para receber os arquivos do ambiente ompremisse (linux).

#retorne ao terminal do linux (ompremisse) e execute o comando.

#Abra uma aba no terminal e execute o comando de inicialização do console GCP na pasta do projeto.
	
	./google-cloud-sdk/bin/gcloud init

#em sequencia será solicitado seu usuario e senha da conta GCP e em sequencia selecione o bucket(boavista_storage1) e zona (us-central1).

#criei na pasta scripts do projeto os script export_price_quote.sh, export_bill_of_materials.sh e export_comp_boss.sh para enviar os arquivos utilizando comandos gsutil com recursividade para melhor desempenho de envio, devido serem poucos arquivos e com volumetria pequena utilizei comandos gsutil para enviar por ser a solução mais rapida quando temos estes quesitos, mas também poderiamos utilizar python dependendendo da demanda.

#após criado executei o comando de execução:

	./export_price_quote.sh &> home/gcp/logs/export_price_quote.log
	./export_bill_of_materials.sh &> home/gcp/logs/export_bill_of_materials.log
	./export_comp_boss.sh &> home/gcp/logs/export_comp_boss.log

# o comando "&> home/gcp/logs/.log" é utilizado para enviar o log gerado pela execuçao do script de ingestão dos dados.

# finalizado o envio dos arquivos para o Google Cloud Storage.

#Consumo de dados Disponibilizado em Dashboard no Data Studio (ferramenta do google).

#O dashboard Está disponibilizado no link abaixo e também será disponibilizado em PDF:

	https://datastudio.google.com/reporting/dc049b15-8306-42e9-b5e3-8e7007dcdbb0

#O Dashboard contém 3 páginas, cada uma consumindo um dos diretorios dos arquivos enviados para o Cloud Storage.

#Fim Do projeto.
